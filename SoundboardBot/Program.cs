﻿using Discord;
using Discord.Audio;
using Discord.Commands;
using NAudio.Wave;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord.Net;
using System.Text.RegularExpressions;
using System.Runtime.InteropServices;
using NAudio;

namespace SoundboardBot
{
    class Program
    {
        //Clients
        static DiscordClient discordClient;
        static IAudioClient audioClient;

        static bool isPlaying = false;
        static bool stfuMode = false;

        static void Main(string[] args)
        {
            Console.WriteLine("Chicken Bot starting...");
            
            //Initialize the client            
            discordClient = new DiscordClient();

            AudioServiceConfigBuilder builder = new AudioServiceConfigBuilder();
            builder.Bitrate = AudioServiceConfig.MaxBitrate;            
            discordClient.UsingAudio(builder.Build());

            CommandServiceConfigBuilder commandBuilder = new CommandServiceConfigBuilder();
            commandBuilder.PrefixChar = '!';
            commandBuilder.HelpMode = HelpMode.Public;
            discordClient.UsingCommands(commandBuilder.Build());

            CreateCommands();

            discordClient.JoinedServer += async (s, e) =>
            {
                Console.WriteLine("Connected to server: {0}", e.Server.Name);
                DoOnInServer(e.Server);
            };

            discordClient.LoggedIn += async (s, e) =>
            {
                Console.WriteLine("Logged in!");

                var appSettings = ConfigurationSettings.AppSettings;

                //Do we have a server saved?
                if (!discordClient.Servers.Any())
                {                    
                    string invitecode = appSettings["discord_invitecode"] ?? "fakecode";

                    //Loop the console till we are good
                    while (true)
                    {
                        Invite invite = await discordClient.GetInvite(invitecode);

                        if (invite.IsRevoked)
                        {
                            if (invitecode.Equals("fakecode"))
                                Console.WriteLine("No invite code was specified in the config. Enter one now, or edit the config xml and reset.");
                            else
                                Console.WriteLine("This invite was already revoked. Enter a new one now, or edit the config xml and reset.");
                            Console.Write(">");
                            invitecode = Console.ReadLine();
                            continue;
                        }
                        else
                        {
                            await invite.Accept();
                            break;
                        }
                    }
                    
                }
                else
                {
                    Server server = discordClient.Servers.First();
                    Console.WriteLine("Already connected to a server: {0}", server.Name);
                    DoOnInServer(server);
                }
                
            };            

            discordClient.ExecuteAndWait(async () =>
            {
                var appSettings = ConfigurationSettings.AppSettings;
                string username = appSettings["discord_account_email"];
                string password = appSettings["discord_account_password"];

                if (username == null || password == null)
                {
                    Console.WriteLine("Email or password was not defined.");
                    Console.WriteLine("Press any key to exit.");
                    Console.ReadKey();
                    Environment.Exit(0);
                }

                Console.WriteLine("Logging in as \"{0}\"...", username);
                try {
                    string result = await discordClient.Connect(username, password);
                }
                catch (HttpException e)
                {
                    Console.WriteLine("Invalid email or password was provided.");
                    Console.WriteLine("Press any key to exit.");
                    Console.ReadLine();
                    Environment.Exit(0);
                }

            });


        }

        static async void DoOnInServer(Server server)
        {
            var appSettings = ConfigurationSettings.AppSettings;
            Channel channel = null;
            if (appSettings["discord_default_voicechannel"] != null)
            {
                if (discordClient.Servers.First().FindChannels(appSettings["discord_default_voicechannel"]).Any())
                {
                    channel = discordClient.Servers.First().FindChannels(appSettings["discord_default_voicechannel"]).First();
                    if (channel.Type != ChannelType.Voice)
                        channel = null;
                    else
                        audioClient = await channel.JoinAudio();
                }

                if (channel == null)
                    Console.WriteLine("Voice channel {0} doesn't exist!", appSettings["discord_default_voicechannel"]);
            }

            //Either no default was specified or the default doesn't exist
            if (channel == null)
            {
                foreach (Channel c in discordClient.Servers.First().AllChannels)
                {
                    if (c.Type == ChannelType.Voice)
                    {
                        audioClient = await c.JoinAudio();
                    }
                }
            }

            await server.DefaultChannel.SendMessage("Chicken Bot is online...");
        }

        static void CreateCommands()
        {
            Console.WriteLine("Creating commands...");

            CommandService commands = discordClient.Services.Get<CommandService>();

            commands.CreateCommand("play")
                .Alias(new string[] { "p", "sb" })
                .Description("plays a sounds from the list")
                .Parameter("Name", ParameterType.Required)
                .Do(async e =>
                {
                    if (!stfuMode && !isPlaying)
                        await PlaySound(e.GetArg("Name"));
                });

            commands.CreateCommand("list")
                .Alias(new string[] { "l", "ls" })
                .Description("Lists all stored sounds")
                .Do(async e =>
                {

                    await e.Channel.SendMessage(String.Format("```\n-----------------\nList of sounds\n-----------------\n{0}```", GetListOfSounds()));

                });

            commands.CreateCommand("vox")
                .Description("Play Half-Life VOX sentence.")
                .Parameter("Sentence", ParameterType.Required)
                .Do(async e =>
                {
                    if (!stfuMode && !isPlaying)
                    {
                        string fuckup = await PlayVox("vox", e.Message.Text.Substring("!vox".Length+1));
                        if (fuckup != null)
                            await e.Channel.SendMessage("Vox playback failed; invalid word: \n" + fuckup);
                    }
                });

            commands.CreateCommand("fvox")
                .Description("Play Half-Life FVOX sentence.")
                .Parameter("Sentence", ParameterType.Required)
                .Do(async e =>
                {
                    if (!stfuMode && !isPlaying)
                    {
                        string fuckup = await PlayVox("fvox", e.Message.Text.Substring("!fvox".Length + 1));
                        if (fuckup != null)
                            await e.Channel.SendMessage("FVox playback failed; invalid word: \n" + fuckup);
                    }
                });

            commands.CreateCommand("hgrunt")
                .Description("Play Half-Life HGRUNT sentence.")
                .Parameter("Sentence", ParameterType.Required)
                .Do(async e =>
                {
                    if (!stfuMode && !isPlaying)
                    {
                        string fuckup = await PlayVox("hgrunt", e.Message.Text.Substring("!hgrunt".Length + 1));
                        if (fuckup != null)
                            await e.Channel.SendMessage("HGrunt playback failed; invalid word: \n" + fuckup);
                    }
                });

            commands.CreateGroup("chickenbot", cgb =>
            {
                cgb.CreateCommand("setgame")
                    .Description("Set the current game Chicken Bot is playing.")
                    .Parameter("GameName", ParameterType.Required)
                    .Do(e =>
                    {
                        discordClient.SetGame(e.GetArg("GameName"));
                    });

                cgb.CreateCommand("cleargame")
                    .Description("Clears the current game Chicken Bot is playing.")                  
                    .Do(e =>
                    {
                        discordClient.SetGame("");
                    });

                cgb.CreateCommand("joinchannel")
                    .Alias(new string[] { "join" })
                    .Description("Chicken Bot will move voice channels.")
                    .Parameter("ChannelName", ParameterType.Required)
                    .Parameter("Silent", ParameterType.Optional)
                    .Do(async e =>
                    {
                        //Find channel
                        Channel channel = discordClient.Servers.First().FindChannels(e.GetArg("ChannelName")).FirstOrDefault();

                        if (channel == null || channel.Type != ChannelType.Voice)
                        {
                            await e.Channel.SendMessage(String.Format("No voice channel named `{0}` found.", e.GetArg("ChannelName")));
                            return;
                        }

                        //Leave if in a channel
                        if (audioClient != null)
                            await audioClient.Channel.LeaveAudio();

                        audioClient = null;

                        //Join new channel
                        audioClient = await channel.JoinAudio();

                        //Play greeting if can
                        if (e.GetArg("Silent") == null)
                            await PlaySound(e.GetArg("greet"));

                    });

                cgb.CreateCommand("joinchannel")
                    .Alias(new string[] { "join"})
                    .Description("Chicken Bot will move voice channels.")
                    .Parameter("ChannelName", ParameterType.Required)
                    .Parameter("Silent", ParameterType.Optional)
                    .Do(async e =>
                    {
                        //Find channel
                        Channel channel = discordClient.Servers.First().FindChannels(e.GetArg("ChannelName")).FirstOrDefault();

                        if (channel == null || channel.Type != ChannelType.Voice)
                        {
                            await e.Channel.SendMessage(String.Format("No voice channel named `{0}` found.", e.GetArg("ChannelName")));
                            return;
                        }

                        //Leave if in a channel
                        if (audioClient != null)
                            await audioClient.Channel.LeaveAudio();

                        audioClient = null;

                        //Join new channel
                        audioClient = await channel.JoinAudio();                        

                        //Play greeting if can
                        if (e.GetArg("Silent") == null)
                            await PlaySound(e.GetArg("greet"));

                    });

                cgb.CreateCommand("leavechannel")
                    .Alias(new string[] { "leave" })
                    .Description("Chicken Bot will leave it's voice channel.")                  
                    .Do(async e =>
                    {
                        if (audioClient != null)
                            await audioClient.Channel.LeaveAudio();

                        audioClient = null;
                    });

                cgb.CreateCommand("mute")
                    .Alias(new string[] { "stfu" })
                    .Description("Mutes chickenbot.")
                    .Parameter("Toggle", ParameterType.Optional)
                    .Do(async e =>
                    {
                        if (e.GetArg("Toggle").Equals("") || e.GetArg("Toggle").Equals("on", StringComparison.OrdinalIgnoreCase))
                            stfuMode = true;
                        else if (e.GetArg("Toggle").Equals("off", StringComparison.OrdinalIgnoreCase))
                            stfuMode = false;

                        await e.Channel.SendMessage(String.Format("Mute is `{0}`.", stfuMode ? "On" : "Off"));
                    });
            });
        } 

        static string GetListOfSounds()
        {
            string list = "";

            string[] files = Directory.GetFiles("./audio/");

            foreach (string str in files)
            {
                if (str.EndsWith(".wav") || str.EndsWith(".mp3") || str.EndsWith(".ogg"))
                {
                    string formatted = str.Replace("./audio/", "");
                    formatted = formatted.Substring(0, formatted.LastIndexOf("."));
                    list += formatted + "\n";
                }
            }

            return list;
        }

        async static Task<Boolean> PlaySound(string filename)
        {            

            string path = null;

            //Figure out he path
            if (File.Exists(String.Format("./audio/{0}.wav", filename)))
                path = String.Format("./audio/{0}.wav", filename);
            else if (File.Exists(String.Format("./audio/{0}.mp3", filename)))
                path = String.Format("./audio/{0}.mp3", filename);
            else if (File.Exists(String.Format("./audio/{0}.ogg", filename)))
                path = String.Format("./audio/{0}.ogg", filename);
            else
                return false;

            isPlaying = true;

            Mp3FileReader reader = new Mp3FileReader(path);
            var pStream = WaveFormatConversionStream.CreatePcmStream(reader);

            var desiredOutputFormat = new WaveFormat(48000, 16, 2);
            using (var converter = new WaveFormatConversionStream(desiredOutputFormat, reader))
            {
                await converter.CopyToAsync(audioClient.OutputStream);
            }              

            //pStream.Seek(0, System.IO.SeekOrigin.Begin);            
            
            //await pStream.CopyToAsync(audioClient.OutputStream);

            reader.Close();

            isPlaying = false;

            return true;
        }

        async static Task<string> PlayVox(string voxFolder, string sentence)
        {
            isPlaying = true;
            bool wasFailure = false;

            sentence = sentence.Replace("\"", "").ToLower();

            string errorSentence = "";

            //Check if all sounds exist for all words
            string[] split = Regex.Split(sentence, @"([\s,\.]+)");

            foreach (String s in split)
            {
                string test = s.Replace(" ", "");

                //Special case for period and comma
                if (test.Equals("."))
                    continue;
                else if (test.Equals(","))
                    continue;
                else if (test.Equals(" "))
                    continue;
                else if (test.Equals(""))
                    continue;

                if (!File.Exists(String.Format("./audio/vox/{0}/{1}.wav", voxFolder, test)))
                {
                    wasFailure = true;
                    sentence = "`" + test + "`";
                    break;
                }
                
            }

            if (wasFailure)
            {
                isPlaying = false;
                return sentence;
            }
            
            //Go through each and play
            foreach (String s in split)
            {
                string test = s.Replace(" ", "");

                //Special case for period and comma
                if (test.Equals("."))
                    test = "_period";
                else if (test.Equals(","))
                    test = "_comma";
                else if (test.Equals(" "))
                    continue;
                else if (test.Equals(""))
                    continue;

                string path = String.Format("./audio/vox/{0}/{1}.wav", voxFolder, test);
         
                var desiredOutputFormat = new WaveFormat(48000, 16, 2);
                using (var reader = new WaveFileReader(path))
                using (var converter = new WaveFormatConversionStream(desiredOutputFormat, reader))
                {
                    await converter.CopyToAsync(audioClient.OutputStream);
                }              
                
            }
            
            isPlaying = false;

            return null;
        }
        
    }
}
